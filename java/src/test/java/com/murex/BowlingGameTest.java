/*
Copyright (c) 2021 Murex

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.murex;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class BowlingGameTest {

    @Ignore
    @Test
    public void acceptance_test() {
        assertEquals(133, BowlingGame.score(1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6));
    }

    @Test
    public void score_20_quand_une_quille_touchée_dans_20_lancées() {
        assertEquals(20, BowlingGame.score(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1));
    }

    @Test
    public void throw_quand_lancé_au_dela_de_20() {
        assertThrows(RuntimeException.class, ()->{BowlingGame.score(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);});
    }

    @Test
    @Ignore
    public void score_12_quand_spare_puis_1_quille() {
        assertEquals(12, BowlingGame.score(9,1,1));
    }
}
