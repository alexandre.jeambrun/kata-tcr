[![Gradle](https://github.com/murex/Kata-BowlingGame/actions/workflows/gradle.yml/badge.svg)](https://github.com/murex/Kata-BowlingGame/actions/workflows/gradle.yml)
[![Maven](https://github.com/murex/Kata-BowlingGame/actions/workflows/maven.yml/badge.svg)](https://github.com/murex/Kata-BowlingGame/actions/workflows/maven.yml)
[![CMake](https://github.com/murex/Kata-BowlingGame/actions/workflows/cmake.yml/badge.svg)](https://github.com/murex/Kata-BowlingGame/actions/workflows/cmake.yml)
[![Go](https://github.com/murex/Kata-BowlingGame/actions/workflows/go.yml/badge.svg)](https://github.com/murex/Kata-BowlingGame/actions/workflows/go.yml)
[![Dotnet](https://github.com/murex/Kata-BowlingGame/actions/workflows/dotnet.yml/badge.svg)](https://github.com/murex/Kata-BowlingGame/actions/workflows/dotnet.yml)
[![Check Markdown links](https://github.com/murex/Kata-BowlingGame/actions/workflows/markdown-link-check.yml/badge.svg)](https://github.com/murex/Kata-BowlingGame/actions/workflows/markdown-link-check.yml)
[![Add contributors](https://github.com/murex/Kata-BowlingGame/actions/workflows/contributors.yml/badge.svg)](https://github.com/murex/Kata-BowlingGame/actions/workflows/contributors.yml)

# Bowling Game Kata

![Bowling Game](./images/BowlingGame.png) <br>
"[Bowling Strike Bullet Isolated](https://pixabay.com/illustrations/bowling-strike-bullet-isolated-3d-3427969/)" by [MasterTux](https://pixabay.com/fr/users/mastertux-470906/) is licenced under [Pixabay License](https://pixabay.com/fr/service/license/)

## Description

Available [here](http://codingdojo.org/kata/Bowling/)

## Getting Started

- [Java](java/GETTING_STARTED.md)
- [C++](cpp/GETTING_STARTED.md)
- [Go](go/GETTING_STARTED.md)
- [C#](csharp/GETTING_STARTED.md)

## Session Quick Retrospective

You can fill it from [here](QuickRetrospective.md)

## Useful Links

### For this Kata

- [Acceptance Test Example 1](http://www.labviewcraftsmen.com/blog/bowling-kata-unit-test-framework#)
- [Acceptance Test Example 2](http://slocums.homestead.com/gamescore.html)
- [Online Bowling Score Calculator](http://www.bowlinggenius.com/)

### General

- [TCR (Test && Commit || Revert) wrapper](tcr/TCR.md) utility
- Handy for any type of turn-based session: [Online Timer](https://agility.jahed.dev/)

## Session Information

### Style & Duration

- 2-hour [Prepared Kata](doc/PreparedKata.md)

### Topic

- Basic TDD

### Focus Points

- Red-Green-refactor
- Baby Steps
- YAGNI

### Source Files

- [Java](java)
- [C++](cpp)
- [Go](go)
- [C#](csharp)

## License

`Kata-BowlingGame` and the accompanying materials are made available
under the terms of the [MIT License](LICENSE.md) which accompanies this
distribution, and is available at the [Open Source site](https://opensource.org/licenses/MIT)

## Acknowledgements

See [ACKNOWLEDGEMENTS.md](ACKNOWLEDGEMENTS.md) for more information.

## Contributors

<table>
<tr>
    <td align="center" style="word-wrap: break-word; width: 150.0; height: 150.0">
        <a href=https://github.com/mengdaming>
            <img src=https://avatars.githubusercontent.com/u/1313765?v=4 width="100;"  style="border-radius:50%;align-items:center;justify-content:center;overflow:hidden;padding-top:10px" alt=Damien Menanteau/>
            <br />
            <sub style="font-size:14px"><b>Damien Menanteau</b></sub>
        </a>
    </td>
    <td align="center" style="word-wrap: break-word; width: 150.0; height: 150.0">
        <a href=https://github.com/philou>
            <img src=https://avatars.githubusercontent.com/u/23983?v=4 width="100;"  style="border-radius:50%;align-items:center;justify-content:center;overflow:hidden;padding-top:10px" alt=Philippe Bourgau/>
            <br />
            <sub style="font-size:14px"><b>Philippe Bourgau</b></sub>
        </a>
    </td>
    <td align="center" style="word-wrap: break-word; width: 150.0; height: 150.0">
        <a href=https://github.com/aatwi>
            <img src=https://avatars.githubusercontent.com/u/11088496?v=4 width="100;"  style="border-radius:50%;align-items:center;justify-content:center;overflow:hidden;padding-top:10px" alt=Ahmad Atwi/>
            <br />
            <sub style="font-size:14px"><b>Ahmad Atwi</b></sub>
        </a>
    </td>
    <td align="center" style="word-wrap: break-word; width: 150.0; height: 150.0">
        <a href=https://github.com/AntoineMx>
            <img src=https://avatars.githubusercontent.com/u/77109701?v=4 width="100;"  style="border-radius:50%;align-items:center;justify-content:center;overflow:hidden;padding-top:10px" alt=AntoineMx/>
            <br />
            <sub style="font-size:14px"><b>AntoineMx</b></sub>
        </a>
    </td>
</tr>
</table>

## Additional Contributors

### For providing initial setup for C#

<table>
<tr>
    <td align="center" style="word-wrap: break-word; width: 150.0; height: 150.0">
        <a href=https://github.com/Tr00d>
            <img src=https://avatars.githubusercontent.com/u/59444272?v=4 width="100;"  style="border-radius:50%;align-items:center;justify-content:center;overflow:hidden;padding-top:10px" alt=Guillaume Faas/>
            <br />
            <sub style="font-size:14px"><b>Guillaume Faas</b></sub>
        </a>
    </td>